package com.app.zapateria.rest.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.app.zapateria.rest.model.dao.IAccesoSistemaDao;
import com.app.zapateria.rest.model.entity.AccesoSistema;

@Service
public class AccesoSistemaServiceImpl implements IAccesoSistemaService{
	
	@Autowired
	private IAccesoSistemaDao accesoSistemaDao;

	@Override
	public AccesoSistema store(AccesoSistema accesoSistema) {
		
		return accesoSistemaDao.save(accesoSistema);
	}

}
