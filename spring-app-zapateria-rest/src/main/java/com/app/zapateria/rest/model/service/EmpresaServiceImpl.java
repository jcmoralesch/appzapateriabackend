package com.app.zapateria.rest.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.zapateria.rest.model.dao.IEmpresaDao;
import com.app.zapateria.rest.model.entity.Empresa;

@Service
public class EmpresaServiceImpl implements IEmpresaService{
	
	@Autowired
	private IEmpresaDao empresaDao;

	@Override
	public Empresa store(Empresa empresa) {
		
		return empresaDao.save(empresa);
	}

}
