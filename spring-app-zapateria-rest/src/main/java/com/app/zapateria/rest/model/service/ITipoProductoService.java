package com.app.zapateria.rest.model.service;

import java.util.List;
import com.app.zapateria.rest.model.entity.TipoProducto;

public interface ITipoProductoService {
	
	public TipoProducto store(TipoProducto tipoProducto);
	public List<TipoProducto> findAll();

}
