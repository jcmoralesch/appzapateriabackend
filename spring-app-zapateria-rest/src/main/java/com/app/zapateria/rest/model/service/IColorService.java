package com.app.zapateria.rest.model.service;

import java.util.List;
import com.app.zapateria.rest.model.entity.Color;

public interface IColorService {
	
	public Color store(Color color);
	public List<Color> findAll();

}
