package com.app.zapateria.rest.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.zapateria.rest.model.dao.ITallaDao;
import com.app.zapateria.rest.model.entity.Talla;

@Service
public class TallaServiceImpl implements ITallaService{
	
	@Autowired
	private ITallaDao tallaDao;

	@Override
	public Talla store(Talla talla) {
		
		return tallaDao.save(talla);
	}

	@Override
	public List<Talla> findAll() {
		
		return tallaDao.findAll();
	}

}
