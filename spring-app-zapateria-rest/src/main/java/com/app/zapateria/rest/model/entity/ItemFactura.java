package com.app.zapateria.rest.model.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "factura_items")
public class ItemFactura implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@DecimalMin(value = "0.00")
	@NotNull
	private Double precioVenta;
	@NotNull
	@Min(value = 0)
	@Column(nullable = false)
	private Integer cantidad;
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler", "cantidadProducto" })
	private CantidadProducto cantidadProducto;
	@DecimalMin(value = "0.00")
	@NotNull
	private Double descuento;
	@DecimalMin(value = "0.00")
	@NotNull
	private Double ganancia;

	public Long getId() {
		return id;
	}

	public CantidadProducto getCantidadProducto() {
		return cantidadProducto;
	}

	public void setCantidadProducto(CantidadProducto cantidadProducto) {
		this.cantidadProducto = cantidadProducto;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getPrecioVenta() {
		return precioVenta;
	}

	public void setPrecioVenta(Double precioVenta) {
		this.precioVenta = precioVenta;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Double getDescuento() {
		return descuento;
	}

	public void setDescuento(Double descuento) {
		this.descuento = descuento;
	}

	public Double getGanancia() {
		return ganancia;
	}

	public void setGanancia(Double ganancia) {
		this.ganancia = ganancia;
	}

	public Double getImporte() {
		return cantidad.doubleValue() * precioVenta;
	}
}
