package com.app.zapateria.rest.model.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.zapateria.rest.model.dao.IIngresoProductoDao;
import com.app.zapateria.rest.model.entity.IngresoProducto;

@Service
public class IngresoProductoServiceImpl implements IIngresoProductoService{
	
	@Autowired
	private IIngresoProductoDao ingresoProductoDao;

	@Override
	public IngresoProducto store(IngresoProducto ingresoProducto) {
		
		return ingresoProductoDao.save(ingresoProducto);
	}

	@Override
	public List<IngresoProducto> getAll() {
	
		return ingresoProductoDao.findAll();
	}

	@Override
	public List<IngresoProducto> findByTrasladoGreaterThan(Integer traslado) {
		
		return ingresoProductoDao.findByTrasladoGreaterThan(traslado);
	}

	@Override
	public IngresoProducto findById(Long id) {

		return ingresoProductoDao.findById(id).orElse(null);
	}

	@Override
	public List<IngresoProducto> findByFechaIngresoBetween(LocalDate fecha1, LocalDate fecha2) {
		
		return ingresoProductoDao.findByFechaIngresoBetween(fecha1, fecha2);
	}

}
