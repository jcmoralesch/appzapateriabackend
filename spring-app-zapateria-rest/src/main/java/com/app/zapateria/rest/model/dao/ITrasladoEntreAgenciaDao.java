package com.app.zapateria.rest.model.dao;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.app.zapateria.rest.model.entity.TrasladoProductoEntreAgencia;

public interface ITrasladoEntreAgenciaDao extends JpaRepository<TrasladoProductoEntreAgencia, Long> {
	
	public List<TrasladoProductoEntreAgencia> findByFechaBetween(LocalDate fecha1,LocalDate fecha2);

}
