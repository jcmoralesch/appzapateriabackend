package com.app.zapateria.rest.model.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.app.zapateria.rest.model.entity.Color;

public interface IColorDao extends JpaRepository<Color, Long> {

}
