package com.app.zapateria.rest.model.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.app.zapateria.rest.model.dao.ITrasladoProductoDao;
import com.app.zapateria.rest.model.entity.TrasladoProducto;

@Service
public class TrasladoProductoServiceImpl implements ITrasladoProductoService{
	
	@Autowired
	private ITrasladoProductoDao trasladoProductoDao;

	@Override
	public TrasladoProducto store(TrasladoProducto trasladoProducto) {
		
		return trasladoProductoDao.save(trasladoProducto) ;
	}

	@Override
	public List<TrasladoProducto> getByDate(LocalDate fecha1, LocalDate fecha2) {
		
		return trasladoProductoDao.findByFechaBetween(fecha1, fecha2);
	}

}
