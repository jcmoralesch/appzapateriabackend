package com.app.zapateria.rest.model.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.app.zapateria.rest.model.dao.IClienteDao;
import com.app.zapateria.rest.model.entity.Cliente;

@Service
public class ClienteServiceImpl implements IClienteService{
	
	@Autowired
	private IClienteDao clienteDao;

	@Override
	@Transactional
	public Cliente store(Cliente cliente) {
		
		return clienteDao.save(cliente);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Cliente> filterByNobreOrApellido(String term) {
		
		return clienteDao.filterByNobreOrApellido(term);
	}


	
	

}
