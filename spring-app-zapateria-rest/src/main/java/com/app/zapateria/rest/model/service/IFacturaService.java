package com.app.zapateria.rest.model.service;

import java.time.LocalDate;
import java.util.List;
import com.app.zapateria.rest.model.entity.Factura;

public interface IFacturaService {
	
	public Factura store(Factura factura);
	public List<Factura> findByFechaOrderByDes(String agencia); 
	public Factura getById(Long id);
	public List<Factura> getByFechaBetween(LocalDate fecha1,LocalDate fecha2);
	public List<Factura> getByFechaAndAgencia(LocalDate fecha1,LocalDate fecha2,String agencia);

}
