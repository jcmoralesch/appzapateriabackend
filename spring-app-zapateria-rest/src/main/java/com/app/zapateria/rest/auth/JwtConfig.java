package com.app.zapateria.rest.auth;

public class JwtConfig {
	
	public static final String LLAVE_SECRETA ="alguna.clave.secreta.123456789";
	
	public static final String RSA_PRIVADA="-----BEGIN RSA PRIVATE KEY-----\n" + 
			"MIIEpAIBAAKCAQEAxAH0mhdaOZ/HZugfnvR6TtRLSi8cJkOpe6rEAFmSBYYe7DhS\n" + 
			"8vIge13jGMr8sxoQuUKHTC0ekiSJkRHREGRjxGymkBOSW7YNgoJ9AnmcVYgZZCEA\n" + 
			"ZiJwlSwPIGrTdTEIyD/xM8CIiz70wBS6Y1UnME8fh2K9btZdw8b/eLfMs065p1+7\n" + 
			"HuVs0Bu5lO1pI0pgqpQdfotUssZwSB0qN/bZ/KsktLzZKy4NV2bV26yrPE5laGJy\n" + 
			"hGdKmPGe3fxLSeg5IaNimr9rFwX0pXOHXiN25AOED5NFiz8LOLnnEFxFPFGm+WMY\n" + 
			"oA4xifHuYBCUkexET02m2TD9+fabBJhU4iRG2wIDAQABAoIBAQDA8GfVUo1m0afX\n" + 
			"3UBbGK5YcFLYaD0eUVZAvFyJN6IsO9Suk9ba/IoKxeVon09WAZ9arWk/C97e3oZn\n" + 
			"vZD8wMUQMh9XDVzxV+g+grIJQKop63mK/Y1F/Dq0LBWLxHv4DNdnPsHwPmJqJwOH\n" + 
			"HCBI7SZRQGF5xDFSLENKFtWc/9fI20hC76xOS1wzEt5+gL4pKY6JK+sIfSAERffD\n" + 
			"t8ZZGvARrdKHYiMPATXJwWsV7zFFY4EUkIs6y0eSGo7yumEQEavF+OMcEvgvCiGH\n" + 
			"mz7nCmNJ/AItFh3Ogfx1yaZ4GxX+ZjWUcJE/3plCsOSCtwUTgDUzbHq0YPBvO4j1\n" + 
			"SZD5m2bJAoGBAPIPMw0GR0yUslN5ANQXrX9gFsRl/8759Sr/0v7+IkGPB8VXb679\n" + 
			"o0ldLPSQNUZ6Md55739PEFRLpJ74sbXbxMXFpPPVUj5/fdOwPe7z4DF4QI7P2AtG\n" + 
			"QjaSj7FzOIAhOYosrVljGGp9iCFvuF+iLbyKY9vB6I7uRJigy/okQ3DlAoGBAM9L\n" + 
			"yutuOjvHRVAOQJrMv3UJUfYtEo3fDFvT34IZygToXNvTbXcQPi+RCYHFplN3SzBK\n" + 
			"es7giObkNx47eE62i6ChnYezR286U6Ev48VJpF7eRyx3uiMCOr6UBXQw7/uoBi4F\n" + 
			"AgLnc4+K4gUViZfiZtLmF/bTiYrsI7FgnBAf8hy/AoGBANqjl/0fuq8VcK2tEm6A\n" + 
			"pJn09RuY9iLB7A1W3gKtEcwdoWtCuwJY33CY1M5io3xxSzIk+YsIoztAyAU/pWUa\n" + 
			"iPYKvB/yg0MobUMUYhy/87U7ZTSZ0A7++zfH0jr6VAKIAC0qhbnDpH4rvRZWjzby\n" + 
			"ilVOuzv4PlM8LIGgXB4pOzFxAoGADOFAsnAe3if7foXkLbobC0KQ/SK/zGDXe70m\n" + 
			"XgcMLlHhsybn3kX8FyiD1rRZKUZ4PX/jMYdx+iaj5hT73Kgq9MgV6aCSB68wEwvO\n" + 
			"sytoMxzMf07RGevBiqXX0OhZgOFnwOQ+w79txrVyZa545h8DFIeHZaavHRqe/R5r\n" + 
			"k/ToLjECgYBDtYTXj39NCsf1tQDOblV07EuAYoX5CKLXm5lcS6tIkrw8Snf6rF5G\n" + 
			"87X5XR8i95jytz/lgpN+DsL6QVXV0jKVnf8uqbgHDF6bL/9IvlhWRsqVh07RnHY5\n" + 
			"Vlw2K5yomitAIu46yGb/Zvgjpypg8dqWxNMADfMM6tUqUBEY+edZnQ==\n" + 
			"-----END RSA PRIVATE KEY-----";
	
	public static final String RSA_PUBLICA="-----BEGIN PUBLIC KEY-----\n" + 
			"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxAH0mhdaOZ/HZugfnvR6\n" + 
			"TtRLSi8cJkOpe6rEAFmSBYYe7DhS8vIge13jGMr8sxoQuUKHTC0ekiSJkRHREGRj\n" + 
			"xGymkBOSW7YNgoJ9AnmcVYgZZCEAZiJwlSwPIGrTdTEIyD/xM8CIiz70wBS6Y1Un\n" + 
			"ME8fh2K9btZdw8b/eLfMs065p1+7HuVs0Bu5lO1pI0pgqpQdfotUssZwSB0qN/bZ\n" + 
			"/KsktLzZKy4NV2bV26yrPE5laGJyhGdKmPGe3fxLSeg5IaNimr9rFwX0pXOHXiN2\n" + 
			"5AOED5NFiz8LOLnnEFxFPFGm+WMYoA4xifHuYBCUkexET02m2TD9+fabBJhU4iRG\n" + 
			"2wIDAQAB\n" + 
			"-----END PUBLIC KEY-----";

}
