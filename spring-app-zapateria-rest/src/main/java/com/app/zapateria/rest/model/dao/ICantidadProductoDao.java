package com.app.zapateria.rest.model.dao;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.app.zapateria.rest.model.entity.Agencia;
import com.app.zapateria.rest.model.entity.CantidadProducto;
import com.app.zapateria.rest.model.entity.Producto;

public interface ICantidadProductoDao extends JpaRepository<CantidadProducto, Long>  {
	@Query("select c from CantidadProducto c where c.producto.codigo like %?1% and c.agencia.nombre=?2 and c.cantidad>0 ")
	public List<CantidadProducto> findProductoFilterByNombre(String term,String nombreAgencia);
	public CantidadProducto findByAgenciaAndProducto(Agencia agencia,Producto producto);
	public List<CantidadProducto> findByAgencia(Agencia agencia);
	@Query("select p from CantidadProducto p where p.producto.codigo=?1 and p.agencia.nombre=?2")
	public CantidadProducto findByProductoAndAgencia(String producto,String agencia);
	@Query("select p from CantidadProducto p where p.agencia.nombre=?1 and p.cantidad=?2")
	public List<CantidadProducto> findByAgenciaAndCantidad(String agencia,Integer cantidad);
	@Query("select p from CantidadProducto p where p.cantidad=?1")
	public List<CantidadProducto> findByCantidadAll(Integer cantidad);
	
	@Query("select p from CantidadProducto p where p.agencia.nombre=?1 and p.cantidad>0")
	public List<CantidadProducto> findProductoByAgenciaAndCantidad(String nombre);
	
	@Query("select p from CantidadProducto p where p.producto.talla.talla=?1 and p.agencia.nombre=?2 and p.cantidad>0")
	public List<CantidadProducto> findByTalla(Integer talla, String agencia);
	
	@Query("select p from CantidadProducto p where p.agencia.nombre=?1 and p.producto.marca.marca=?2 and p.cantidad>0")
	public List<CantidadProducto> findByMarca(String agencia,String marca);
	
	@Query("select p from CantidadProducto p where p.agencia.nombre=?1 and p.producto.talla.talla=?2 and p.producto.marca.marca=?3 and p.cantidad>0")
	public List<CantidadProducto> findByAgenciaTallaMarca(String agencia,Integer talla,String marca);
	
	@Query("select p from CantidadProducto p where p.agencia.nombre=?1 and p.producto.categoria.categoria=?2")
	public List<CantidadProducto> findByAgenciaCategoria(String agencia,String categoria);
	
	@Query("select p from CantidadProducto p where p.agencia.nombre=?1 and p.producto.talla.talla=?2 and p.producto.categoria.categoria=?3")
	public List<CantidadProducto> findByAgenciaTallaCategoria(String agencia,Integer talla,String categoria);
	
	@Query("select p from CantidadProducto p where p.agencia.nombre=?1 and p.producto.marca.marca=?2 and p.producto.categoria.categoria=?3")
	public List<CantidadProducto> findByAgenciaMarcaCategoria(String agencia,String marca,String categoria);
	
	@Query("select p from CantidadProducto p where p.agencia.nombre=?1 and p.producto.talla.talla=?2 and p.producto.marca.marca=?3  and p.producto.categoria.categoria=?4")
	public List<CantidadProducto> findByAgenciaTallaCategoriaMarca(String agencia,Integer talla,String marca,String categoria);
	
	@Query("select p from CantidadProducto p where p.agencia.nombre=?1 and p.producto.tipoProducto.tipoProducto=?2")
	public List<CantidadProducto> findByAgenciaTipoProducto(String agencia,String tipoProducto);
	
	@Query("select p from CantidadProducto p where p.agencia.nombre=?1 and p.producto.talla.talla=?2 and p.producto.tipoProducto.tipoProducto=?3")
	public List<CantidadProducto> findByAgenciaTallaTipoProducto(String agencia,Integer talla,String tipoProducto);
	
	@Query("select p from CantidadProducto p where p.agencia.nombre=?1 and p.producto.marca.marca=?2 and p.producto.tipoProducto.tipoProducto=?3")
	public List<CantidadProducto> findByAgenciaMarcaTipoProducto(String agencia,String marca,String tipoProducto);
	
	@Query("select p from CantidadProducto p where p.agencia.nombre=?1 and p.producto.categoria.categoria=?2 and p.producto.tipoProducto.tipoProducto=?3")
	public List<CantidadProducto> findByAgenciaCategoriaTipoProducto(String agencia,String categoria,String tipoProducto);
	
	@Query("select p from CantidadProducto p where p.agencia.nombre=?1 and p.producto.talla.talla=?2 and p.producto.marca.marca=?3 and p.producto.tipoProducto.tipoProducto=?4")
	public List<CantidadProducto> findByAgenciaTallaMarcaTipoProducto(String agencia,Integer talla,String marca,String tipoProducto); 
	
	@Query("select p from CantidadProducto p where p.agencia.nombre=?1 and p.producto.marca.marca=?2 and p.producto.categoria.categoria=?3 and p.producto.tipoProducto.tipoProducto=?4")
	public List<CantidadProducto> findByAgenciaMarcaCategoriaTipoProducto(String agencia,String marca,String categoria,String tipoProducto);
	
	@Query("select p from CantidadProducto p where p.agencia.nombre=?1 and p.producto.talla.talla=?2 and p.producto.marca.marca=?3 and p.producto.categoria.categoria=?4 and p.producto.tipoProducto.tipoProducto=?5")
	public List<CantidadProducto> findByAgenciaTallaMarcaCategoriaTipo(String agencia,Integer talla,String marca,String categoria, String tipo);
	
	@Query("select p from CantidadProducto p where p.agencia.nombre=?1 and p.producto.talla.talla=?2 and p.producto.categoria.categoria=?3 and p.producto.tipoProducto.tipoProducto=?4")
	public List<CantidadProducto> findByAgenciaTallaCategoriaTipoProducto(String agencia,Integer talla,String categoria,String tipoProducto);
	
	@Query("select p from CantidadProducto p where p.agencia.nombre=?1 and p.producto.color.color=?2")
	public List<CantidadProducto> findByAgenciaColor(String agencia,String color);
	
	@Query("select p from CantidadProducto p where p.agencia.nombre=?1 and p.producto.talla.talla=?2 and p.producto.color.color=?3")
	public List<CantidadProducto> findByAgenciaTallaColor(String agencia,Integer talla,String color);
	
	@Query("select p from CantidadProducto p where p.agencia.nombre=?1 and p.producto.marca.marca=?2 and p.producto.color.color=?3")
	public List<CantidadProducto> findByAgenciaMarcaColor(String agencia,String marca,String color);
	
	@Query("select p from CantidadProducto p where p.agencia.nombre=?1 and p.producto.categoria.categoria=?2 and p.producto.color.color=?3")
	public List<CantidadProducto> findByAgenciaCategoriaColor(String agencia,String categoria,String color);
	
	@Query("select p from CantidadProducto p where p.agencia.nombre=?1 and p.producto.tipoProducto.tipoProducto=?2 and p.producto.color.color=?3")
	public List<CantidadProducto> findByAgenciaTipoProductoColor(String agencia,String tipoProducto,String color);
	
	@Query("select p from CantidadProducto p where p.agencia.nombre=?1 and p.producto.talla.talla=?2 and p.producto.categoria.categoria=?3 and p.producto.color.color=?4")
	public List<CantidadProducto> findByAgenciaTallaCategoriaColor(String agencia,Integer talla,String categoria,String color);
	
	@Query("select p from CantidadProducto p where p.agencia.nombre=?1 and p.producto.talla.talla=?2 and p.producto.categoria.categoria=?3 and p.producto.tipoProducto.tipoProducto=?4 and p.producto.color.color=?5")
	public List<CantidadProducto> findByAgenciaTallaCategoriaTipoProdColor(String agencia,Integer talla, String categoria,String tipoProducto,String color);
	
	@Query("select p from CantidadProducto p where p.agencia.nombre=?1 and p.producto.categoria.categoria=?2 and p.producto.tipoProducto.tipoProducto=?3 and p.producto.color.color=?4")
	public List<CantidadProducto> findByAgenciaCategoriaTipoProductoColor(String agencia,String categoria,String tipoProducto,String color);
	
	@Query("select p from CantidadProducto p where p.agencia.nombre=?1 and p.producto.marca.marca=?2 and p.producto.categoria.categoria=?3 and p.producto.color.color=?4")
	public List<CantidadProducto> findByAgenciaMarcaCategoriaColor(String agencia,String marca,String categoria,String color);


}
