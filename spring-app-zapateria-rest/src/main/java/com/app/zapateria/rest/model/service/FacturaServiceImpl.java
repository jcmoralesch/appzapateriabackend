package com.app.zapateria.rest.model.service;

import java.time.LocalDate;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.app.zapateria.rest.model.dao.IFacturaDao;
import com.app.zapateria.rest.model.entity.Factura;

@Service
public class FacturaServiceImpl implements IFacturaService{
	
	@Autowired
	private IFacturaDao facturaDao;

	@Override
	@Transactional
	public Factura store(Factura factura) {
		
		return facturaDao.save(factura);
	}

	@Override
	@Transactional(readOnly = true)
	public Factura getById(Long id) {

		return facturaDao.findById(id).orElse(null);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Factura> findByFechaOrderByDes(String agencia) {
		return facturaDao.findByFechaOrderByDes(agencia);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Factura> getByFechaBetween(LocalDate fecha1, LocalDate fecha2) {
		
		return facturaDao.findByFechaBetween(fecha1, fecha2);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Factura> getByFechaAndAgencia(LocalDate fecha1, LocalDate fecha2, String agencia) {
		
		return facturaDao.findByFechaAndAgencia(fecha1, fecha2, agencia);
	}

}
