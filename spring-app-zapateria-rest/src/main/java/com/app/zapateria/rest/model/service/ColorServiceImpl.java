package com.app.zapateria.rest.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.zapateria.rest.model.dao.IColorDao;
import com.app.zapateria.rest.model.entity.Color;

@Service
public class ColorServiceImpl implements IColorService{
	
	@Autowired
	private IColorDao colorDao;

	@Override
	public Color store(Color color) {
		
		return colorDao.save(color);
	}

	@Override
	public List<Color> findAll() {
		
		return colorDao.findAll();
	}

}
