package com.app.zapateria.rest.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.app.zapateria.rest.model.entity.CantidadProducto;
import com.app.zapateria.rest.model.entity.Factura;
import com.app.zapateria.rest.model.entity.ItemFactura;
import com.app.zapateria.rest.model.entity.Usuario;
import com.app.zapateria.rest.model.service.ICantidadProductoService;
import com.app.zapateria.rest.model.service.IFacturaService;
import com.app.zapateria.rest.model.service.IUsuarioService;

@RestController
@RequestMapping("/api")
public class FacturaController {

	@Autowired
	private IUsuarioService usuarioService;
	@Autowired
	private ICantidadProductoService cantidadProductoService;
	@Autowired
	private IFacturaService facturaService;

	@GetMapping("/factura/filtrar-productos/{term}")
	@ResponseStatus(HttpStatus.OK)
	private List<CantidadProducto> findByTerm(@PathVariable String term, OAuth2Authentication authentication) {
		Usuario usuario = usuarioService.findByUsername(authentication.getName());
		return cantidadProductoService.findProductoFilterByNombre(term, usuario.getPersonal().getAgencia().getNombre());
	}

	@PostMapping("/factura")
	private ResponseEntity<?> store(@Valid @RequestBody Factura factura, BindingResult errores,
			OAuth2Authentication authentication) {
		Map<String, Object> response = new HashMap<>();
		Factura facturaNew = new Factura();
		CantidadProducto cantidadProducto = null;
		Usuario usuario = usuarioService.findByUsername(authentication.getName());
		List<ItemFactura> it = factura.getItems();
		List<ItemFactura> pv = new ArrayList<ItemFactura>();

		if (errores.hasErrors()) {
			List<String> errors = errores.getFieldErrors().stream()
					.map(err -> "El campo " + err.getField() + " " + err.getDefaultMessage())
					.collect(Collectors.toList());

			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}

		try {

			for (ItemFactura itemFactura : it) {
				cantidadProducto = cantidadProductoService.findByAgenciaAndProducto(usuario.getPersonal().getAgencia(),
						itemFactura.getCantidadProducto().getProducto());

				cantidadProducto.setCantidad(cantidadProducto.getCantidad() - itemFactura.getCantidad());

				cantidadProductoService.store(cantidadProducto);

				if (itemFactura.getPrecioVenta() == 0) {
					itemFactura.setPrecioVenta(cantidadProducto.getProducto().getPrecioVenta());	
					itemFactura.setDescuento(0.00);
					itemFactura.setGanancia((cantidadProducto.getProducto().getPrecioVenta()-cantidadProducto.getProducto().getPrecio())*itemFactura.getCantidad());
					
				} else {
					itemFactura.setPrecioVenta(itemFactura.getPrecioVenta());
					itemFactura.setDescuento((cantidadProducto.getProducto().getPrecioVenta()-itemFactura.getPrecioVenta())*itemFactura.getCantidad());
					System.out.println(cantidadProducto.getProducto().getPrecioVenta()-itemFactura.getPrecioVenta());
					itemFactura.setGanancia((itemFactura.getPrecioVenta()-cantidadProducto.getProducto().getPrecio())*itemFactura.getCantidad());
				}

				pv.add(itemFactura);

			}
			
			factura.setItems(pv);
			factura.setUsuario(usuario);
			
			facturaNew=facturaService.store(factura);

		} catch (DataAccessException e) {
			
			response.put("mensaje","Error no se pudo registrar la venta, debibo a que hubo problemas en el servidor");
			response.put("err", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);

		}
		
		response.put("factura",facturaNew);
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);
	}
	
	@GetMapping("/factura")
	@ResponseStatus(HttpStatus.OK)
	private List<Factura> getAll(OAuth2Authentication authentication){
		
		Usuario usuario= usuarioService.findByUsername(authentication.getName());
		return facturaService.findByFechaOrderByDes(usuario.getPersonal().getAgencia().getNombre());
	}
	
	@GetMapping("/factura/{id}")
	@ResponseStatus(HttpStatus.OK)
	private Factura getById(@PathVariable Long id) {
		return facturaService.getById(id);
	}
	
	@GetMapping("/factura/{fecha1}/{fecha2}/{agencia}")
	private List<Factura> findByFechasAndAgencias(@PathVariable String fecha1,@PathVariable String fecha2, @PathVariable String agencia){
		DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
		LocalDate date1 = LocalDate.parse(fecha1, inputFormatter);
		LocalDate date2 = LocalDate.parse(fecha2,inputFormatter);
		
		if(agencia.equals("undefined")) {
			agencia="";
		}
		
		if(agencia=="") {
			return facturaService.getByFechaBetween(date1, date2);
		}
		
		return facturaService.getByFechaAndAgencia(date1, date2, agencia);
	}

}
