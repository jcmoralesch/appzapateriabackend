package com.app.zapateria.rest.model.dao;

import org.springframework.data.repository.CrudRepository;

import com.app.zapateria.rest.model.entity.Role;

public interface IRoleDao extends CrudRepository<Role, Long> {

}
