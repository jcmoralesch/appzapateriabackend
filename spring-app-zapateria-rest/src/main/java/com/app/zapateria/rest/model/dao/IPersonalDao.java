package com.app.zapateria.rest.model.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.app.zapateria.rest.model.entity.Personal;

public interface IPersonalDao extends JpaRepository<Personal, Long> {

}
