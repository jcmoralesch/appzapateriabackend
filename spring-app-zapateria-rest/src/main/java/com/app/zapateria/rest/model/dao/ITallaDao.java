package com.app.zapateria.rest.model.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.app.zapateria.rest.model.entity.Talla;

public interface ITallaDao extends JpaRepository<Talla, Long> {

}
