package com.app.zapateria.rest.model.service;

import java.util.List;
import com.app.zapateria.rest.model.entity.Producto;

public interface IProductoService {
	
	public Producto store(Producto producto);
	public Producto findById(Long id);
	public List<Producto> getAll();
	
}
