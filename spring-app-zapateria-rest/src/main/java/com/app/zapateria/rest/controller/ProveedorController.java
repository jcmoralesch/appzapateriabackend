package com.app.zapateria.rest.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.app.zapateria.rest.model.entity.Empresa;
import com.app.zapateria.rest.model.entity.Proveedor;
import com.app.zapateria.rest.model.service.IProveedorService;

@RestController
@CrossOrigin({"http://localhost:4200","*"})
@RequestMapping("/api")
public class ProveedorController {
	
	@Autowired
	private IProveedorService proveedorService;
	
	@PostMapping("/proveedor")
	private ResponseEntity<?> store(@Valid @RequestBody Proveedor proveedor,BindingResult errores) {
		 
		Empresa empresa = new Empresa();
		Proveedor proveedorNew = new Proveedor();
		Map<String, Object> response = new HashMap<>();
		
		if(errores.hasErrors()) {
			List<String> errors=errores.getFieldErrors().stream().map(err-> "El campo " + err.getField() + " " + 
				     err.getDefaultMessage()).collect(Collectors.toList());
			
			response.put("errors",errors);
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.BAD_REQUEST);
		}
		
		try {
			empresa.setNombre(proveedor.getEmpresa().getNombre().toUpperCase());
			empresa.setDireccion(proveedor.getEmpresa().getDireccion().toUpperCase());
			empresa.setTelefono(proveedor.getEmpresa().getTelefono());
			
			proveedor.setIdentificacion(proveedor.getIdentificacion().toUpperCase());
			proveedor.setNombre(proveedor.getNombre().toUpperCase());
			proveedor.setApellido(proveedor.getApellido().toUpperCase());
			proveedor.setEmpresa(empresa);
			
			proveedorNew=proveedorService.store(proveedor,empresa);
		}catch(DataIntegrityViolationException ex) {
	
			 response.put("mensaje", "Error al registrar");
		     response.put("err","Esta ingresando una identificacion o nombre de empresa ya registrada");
		     
		     return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}catch(DataAccessException e) {
			response.put("mensaje","Error al realizar el insert en la BBDD");
			response.put("err",e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}	
		
		response.put("mensaje","Registro ingresada con éxito");
		response.put("proveedor",proveedorNew);
		
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);
	}
	
	@GetMapping("/proveedor/{page}")
	private Page<Proveedor> getAll(@PathVariable Integer page){
		
		return proveedorService.getAllByStatus("A", PageRequest.of(page, 10));
	}
	
	@GetMapping("/proveedor")
	private List<Proveedor> getAll(){
		return proveedorService.getAllByStatus("A");
	}
	
	@GetMapping("/proveedor/find/{id}")
	private Proveedor getById(@PathVariable Long id) {
		return proveedorService.getById(id);
	}
	
	@PutMapping("/proveedor/{id}")
	private ResponseEntity<?> update(@Valid @PathVariable Long id, @RequestBody Proveedor proveedor, BindingResult errores){
		Map<String, Object> response= new HashMap<>();
		
	    Proveedor proveedorFinded=proveedorService.getById(id);  
	    Empresa empresaFinded= proveedorFinded.getEmpresa();
	    Proveedor proveedorUpdated=new Proveedor();
	    
	    if(errores.hasErrors()) {
			List<String> errors=errores.getFieldErrors().stream().map(err-> "El campo " + err.getField() + " " + 
				     err.getDefaultMessage()).collect(Collectors.toList());
			
			response.put("errors",errors);
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.BAD_REQUEST);
		}
	    
	    try {
			empresaFinded.setNombre(proveedor.getEmpresa().getNombre().toUpperCase());
			empresaFinded.setDireccion(proveedor.getEmpresa().getDireccion().toUpperCase());
			empresaFinded.setTelefono(proveedor.getEmpresa().getTelefono());
			
			proveedorFinded.setIdentificacion(proveedor.getIdentificacion().toUpperCase());
			proveedorFinded.setNombre(proveedor.getNombre().toUpperCase());
			proveedorFinded.setApellido(proveedor.getApellido().toUpperCase());
			proveedorFinded.setTelefono(proveedor.getTelefono());
			proveedorFinded.setEmail(proveedor.getEmail());
			proveedorFinded.setEmpresa(empresaFinded);
			
			proveedorUpdated=proveedorService.store(proveedorFinded,empresaFinded);
		}catch(DataIntegrityViolationException ex) {
	
			 response.put("mensaje", "Error al registrar");
		     response.put("err","Esta ingresando una identificacion o nombre de empresa ya registrada");
		     
		     return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}catch(DataAccessException e) {
			response.put("mensaje","Error al realizar el insert en la BBDD");
			response.put("err",e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}		
		response.put("mensaje","Registro ingresada con éxito");
		response.put("proveedor",proveedorUpdated);	
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);    
	}
	
	@PutMapping("proveedor/update/{id}")
	private Proveedor updateStatus(@PathVariable Long id,@RequestBody Proveedor proveedor) {
		Proveedor proveedorUpdated=proveedorService.getById(id);
		proveedorUpdated.setStatus("C");
		return proveedorService.update(proveedorUpdated);
	}

}
