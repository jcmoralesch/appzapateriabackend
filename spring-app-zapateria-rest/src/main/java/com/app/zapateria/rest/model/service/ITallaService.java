package com.app.zapateria.rest.model.service;

import java.util.List;
import com.app.zapateria.rest.model.entity.Talla;

public interface ITallaService {
	
	public Talla store(Talla talla);
	public List<Talla> findAll();

}
