package com.app.zapateria.rest.model.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.app.zapateria.rest.model.entity.Marca;

public interface IMarcaDao extends JpaRepository<Marca, Long> {

}
