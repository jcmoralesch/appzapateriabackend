package com.app.zapateria.rest.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.app.zapateria.rest.model.entity.Role;
import com.app.zapateria.rest.model.service.IRoleService;

@RestController
@CrossOrigin(origins = {"http://localhost:4200","*"})
@RequestMapping("/api")
public class RoleController {
	
	@Autowired
	private IRoleService roleService;
	
	@GetMapping("/role")
	private List<Role> getAll(){
		return roleService.getAll();
	}

}
