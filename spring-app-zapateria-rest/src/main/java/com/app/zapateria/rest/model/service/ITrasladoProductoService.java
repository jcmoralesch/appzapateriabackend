package com.app.zapateria.rest.model.service;

import java.time.LocalDate;
import java.util.List;

import com.app.zapateria.rest.model.entity.TrasladoProducto;

public interface ITrasladoProductoService {
	
	public TrasladoProducto store(TrasladoProducto trasladoProducto);
	public List<TrasladoProducto> getByDate(LocalDate fecha1,LocalDate fecha2);

}
