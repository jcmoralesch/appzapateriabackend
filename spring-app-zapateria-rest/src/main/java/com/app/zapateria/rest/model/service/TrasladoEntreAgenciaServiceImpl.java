package com.app.zapateria.rest.model.service;

import java.time.LocalDate;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.zapateria.rest.model.dao.ICantidadProductoDao;
import com.app.zapateria.rest.model.dao.ITrasladoEntreAgenciaDao;
import com.app.zapateria.rest.model.entity.CantidadProducto;
import com.app.zapateria.rest.model.entity.TrasladoProductoEntreAgencia;

@Service
public class TrasladoEntreAgenciaServiceImpl implements ITrasladoEntreAgenciaService{
	
	@Autowired
	private ITrasladoEntreAgenciaDao trasladoDao;
	@Autowired
	private ICantidadProductoDao cantidadProductoDao;

	@Override
	@Transactional
	public TrasladoProductoEntreAgencia store(TrasladoProductoEntreAgencia trasladoProductoEntreAgencia,
			CantidadProducto agenciaOrigen,CantidadProducto agenciaDestino) {
		
		cantidadProductoDao.save(agenciaOrigen);
		cantidadProductoDao.save(agenciaDestino);
		
		return trasladoDao.save(trasladoProductoEntreAgencia);
	}

	@Override
	@Transactional(readOnly = true)
	public List<TrasladoProductoEntreAgencia> getByDate(LocalDate fecha1, LocalDate fecha2) {
		
		return trasladoDao.findByFechaBetween(fecha1, fecha2);
	}
	
	

}
