package com.app.zapateria.rest.model.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.app.zapateria.rest.model.dao.IMarcaDao;
import com.app.zapateria.rest.model.entity.Marca;

@Service
public class MarcaServiceImpl implements IMarcaService{
	
	@Autowired
	private IMarcaDao marcaDao;

	@Override
	public Marca store(Marca marca) {
		
		return marcaDao.save(marca);
	}

	@Override
	public List<Marca> findAll() {
		
		return marcaDao.findAll();
	}

}
