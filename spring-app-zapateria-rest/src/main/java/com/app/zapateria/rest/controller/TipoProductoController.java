package com.app.zapateria.rest.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.app.zapateria.rest.model.entity.TipoProducto;
import com.app.zapateria.rest.model.service.ITipoProductoService;

@RestController
@CrossOrigin({"http://localhost:4200","*"})
@RequestMapping("/api")
public class TipoProductoController {
	
	@Autowired
	private ITipoProductoService tipoProductoService;
	
	@PostMapping("/tipo-producto")
	private ResponseEntity<?> store(@Valid @RequestBody TipoProducto tipoProducto, BindingResult errores){
		
		Map<String,Object> response=new HashMap<>();
		TipoProducto tipoProductoNew= new TipoProducto();
		
		if(errores.hasErrors()) {
			   List<String> errors=errores.getFieldErrors().stream().map(err-> "El campo " + err.getField() + " " + 
					     err.getDefaultMessage()).collect(Collectors.toList());
			   
			   response.put("errors", errors);
			   return new ResponseEntity<Map<String,Object>>(response,HttpStatus.BAD_REQUEST);
		   }
		   
		   tipoProducto.setTipoProducto(tipoProducto.getTipoProducto().toUpperCase());
		 
		   try {
			  
			   tipoProductoNew=tipoProductoService.store(tipoProducto);	   
		   }
		   catch(DataIntegrityViolationException ex) {
				response.put("mensaje", "Error al registrar");
				response.put("err", tipoProducto.getTipoProducto().concat(" Ya esta registrada"));
				
				return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
			}
			catch(DataAccessException e) {
				response.put("mensaje", "Error al realizar el registro en la BBDD");
				response.put("err",e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
				
				return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
			}
		   
		   response.put("tipoProducto",tipoProductoNew);
		   
		   return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);	
	}
	
	@GetMapping("/tipo-producto")
	private List<TipoProducto> getAll(){
		return tipoProductoService.findAll();
	}
	

}
