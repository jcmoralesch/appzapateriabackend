package com.app.zapateria.rest.model.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.zapateria.rest.model.entity.Categoria;

public interface ICategoriaDao extends JpaRepository<Categoria, Long> {

}
