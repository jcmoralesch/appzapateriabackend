package com.app.zapateria.rest.model.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "traslado_entre_agencias")
public class TrasladoProductoEntreAgencia implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NotNull
	@Min(value = 0)
	private Integer cantidad;
	private LocalDate fecha;
	private LocalTime hora;
	@ManyToOne
	private Agencia agenciaOrigen;
	@ManyToOne
	private Agencia agenciaDestino;
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler", "cantidadProducto" })
	private CantidadProducto cantidadProducto;
	@ManyToOne
	private Usuario usuario;

	@PrePersist
	private void prePersist() {
		fecha = LocalDate.now();
		hora = LocalTime.now();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public LocalTime getHora() {
		return hora;
	}

	public void setHora(LocalTime hora) {
		this.hora = hora;
	}

	public Agencia getAgenciaOrigen() {
		return agenciaOrigen;
	}

	public void setAgenciaOrigen(Agencia agenciaOrigen) {
		this.agenciaOrigen = agenciaOrigen;
	}

	public Agencia getAgenciaDestino() {
		return agenciaDestino;
	}

	public void setAgenciaDestino(Agencia agenciaDestino) {
		this.agenciaDestino = agenciaDestino;
	}

	public CantidadProducto getCantidadProducto() {
		return cantidadProducto;
	}

	public void setCantidadProducto(CantidadProducto cantidadProducto) {
		this.cantidadProducto = cantidadProducto;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}
