package com.app.zapateria.rest.model.service;

import java.util.List;
import com.app.zapateria.rest.model.entity.Marca;

public interface IMarcaService {
	
	public Marca store(Marca marca);
	public List<Marca> findAll();

}
