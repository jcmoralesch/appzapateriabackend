package com.app.zapateria.rest.model.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.zapateria.rest.model.entity.Producto;

public interface IProductoDao extends JpaRepository<Producto, Long> {
	
	@Query("select p from Producto p order by p.id desc")
	public List<Producto> findAllOrderByIdDesc();	

}
