package com.app.zapateria.rest.model.service;

import java.time.LocalDate;
import java.util.List;

import com.app.zapateria.rest.model.entity.CantidadProducto;
import com.app.zapateria.rest.model.entity.TrasladoProductoEntreAgencia;

public interface ITrasladoEntreAgenciaService {
	
	public TrasladoProductoEntreAgencia store(TrasladoProductoEntreAgencia trasladoProductoEntreAgencia,
			CantidadProducto agenciaOrigen,CantidadProducto agenciaDestino);
    public List<TrasladoProductoEntreAgencia> getByDate(LocalDate fecha1,LocalDate fecha2);
}
