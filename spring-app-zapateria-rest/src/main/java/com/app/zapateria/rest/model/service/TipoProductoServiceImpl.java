package com.app.zapateria.rest.model.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.app.zapateria.rest.model.dao.ITipoProductoDao;
import com.app.zapateria.rest.model.entity.TipoProducto;

@Service
public class TipoProductoServiceImpl implements ITipoProductoService{
	
	@Autowired
	private ITipoProductoDao tipoProductoDao;

	@Override
	public TipoProducto store(TipoProducto tipoProducto) {
		
		return tipoProductoDao.save(tipoProducto);
	}

	@Override
	public List<TipoProducto> findAll() {
		
		return tipoProductoDao.findAll();
	}

}
