package com.app.zapateria.rest.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.app.zapateria.rest.model.entity.CantidadProducto;
import com.app.zapateria.rest.model.service.IAgenciaService;
import com.app.zapateria.rest.model.service.ICantidadProductoService;

@RestController
@CrossOrigin(origins = {"http://localhost:4200" ,"*"})
@RequestMapping("/api")
public class CantidadProductoController {
	
	@Autowired
	private ICantidadProductoService cantidadProductoService;
	@Autowired
	private IAgenciaService agenciaService;
	
	@PostMapping("/cantidad-producto")
	private  ResponseEntity<?> store(@Valid @RequestBody CantidadProducto cantidadProducto,
			                        BindingResult errores,OAuth2Authentication autentication){
		Map<String, Object> response= new HashMap<>();
		CantidadProducto cantidadProductoNew= new CantidadProducto();
		
		if(errores.hasErrors()) {
			List<String> errors=errores.getFieldErrors().stream().map(err-> "El campo " + err.getField() + " " + 
				     err.getDefaultMessage()).collect(Collectors.toList());
			
			response.put("errors", errors);
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.BAD_REQUEST);
		}
		
		try {
			cantidadProductoNew = cantidadProductoService.store(cantidadProducto);
		}catch(DataAccessException e) {
			response.put("mensaje", "Error al realizar el registro en la BBDD");
			response.put("err",e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("cantidadProducto", cantidadProductoNew);
		
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);
	}
	
	@GetMapping("/cantidad-producto/{nombre}")
	private List<CantidadProducto> getAll(@PathVariable String nombre,OAuth2Authentication autentication){
		return cantidadProductoService.findByAgencia(agenciaService.findByNombre(nombre));
	}
	
	@GetMapping("/cantidad-producto/agencia/cant/{agencia}")
	private List<CantidadProducto> getByAngenciaAndCantidad(@PathVariable String agencia){
		
		return cantidadProductoService.getProductoByAgenciaAndCantidad(agencia);
	}
	
	@GetMapping("/cantidad-producto")
	private List<CantidadProducto> getAll(){
		return cantidadProductoService.getAll();
	}
	
	@GetMapping("/cantidad-producto/cantidad/{cantidad}")
	private List<CantidadProducto> getCantidadAll(@PathVariable Integer cantidad){
		return cantidadProductoService.getByCantidadAll(cantidad);
	}
	
	@GetMapping("/cantidad-producto/{agencia}/{cantidad}")
	private List<CantidadProducto> getByAgencianAndCantidad(@PathVariable Integer cantidad,@PathVariable String agencia){
		return cantidadProductoService.getByAgenciaAndCantidad(agencia, cantidad);
	}
	
	
	
	@GetMapping("/cantidad-producto/filtrar/{talla}/{marca}/{agencia}/{categoria}/{tipoProducto}/{color}")
	private List<CantidadProducto> getProductoByFilter(@PathVariable String talla,@PathVariable String marca,
			                     @PathVariable String agencia, @PathVariable String categoria,
			                     @PathVariable String tipoProducto,@PathVariable String color){
		
		if(talla.equals("undefined")) {
			talla="";
		}
		if(marca.equals("undefined")) {
			marca="";
		}
		if(categoria.equals("undefined")) {
			categoria="";
		}
		if(tipoProducto.equals("undefined")) {
			tipoProducto="";
		}
		if(color.equals("undefined")) {
			color="";
		}
		
		
		
       if(talla!="" && marca.equals("") && categoria.equals("") && tipoProducto.equals("") && color.equals("") ) {
    	   return cantidadProductoService.getByTalla(Integer.parseInt(talla),agencia); 
		}
       
       if(talla.equals("") && categoria.equals("") && tipoProducto.equals("") && marca!="" && color.equals("")) {
    	   return cantidadProductoService.getByMarca(agencia,marca);
       }
       
       if(talla!="" && marca!="" && categoria.equals("") && tipoProducto.equals("") && color.equals("")) {
    	   return cantidadProductoService.getByAgenciaTallaMarca(agencia,Integer.parseInt(talla),marca);
       }
       
       if(talla.equals("")&& marca.equals("") && tipoProducto.equals("") && categoria!="" && color.equals("")) {
    	   return cantidadProductoService.getByAgenciaCategoria(agencia, categoria);
       }
       
       if(talla!="" && categoria!="" && marca.equals("") && tipoProducto.equals("") && color.equals("")) {
    	   return cantidadProductoService.getByAgenciaTallaCategoria(agencia, Integer.parseInt(talla), categoria);
       }
       
       if(talla.equals("") && marca!="" && categoria!="" && tipoProducto.equals("") && color.equals("")) {
    	   return cantidadProductoService.getByAgenciaMarcaCategoria(agencia, marca, categoria);
       }
       
       if(talla!="" && categoria!="" && marca!="" && tipoProducto.equals("") && color.equals("")) {
    	  return cantidadProductoService.getByAgenciaTallaCategoriaMarca(agencia, Integer.parseInt(talla),marca, categoria);  
       }
       
       if(talla.equals("") && marca.equals("") && categoria.equals("") && tipoProducto!="" && color.equals("")) {
    	   return cantidadProductoService.getByAgenciaTipoProducto(agencia, tipoProducto);
       }
       
       if(talla!="" && marca.equals("") && categoria.equals("") && tipoProducto!="" && color.equals("")) {
    	   return cantidadProductoService.getByAgenciaTallaTipoProducto(agencia,Integer.parseInt(talla), tipoProducto);
       }
       
       if(talla.equals("") && marca!="" && categoria.equals("") && tipoProducto!="" && color.equals("")) {
    	   return cantidadProductoService.getByAgenciaMarcaTipoProducto(agencia, marca, tipoProducto);
       }
       
       if(talla.equals("") && marca.equals("") && categoria!="" && tipoProducto!="" && color.equals("")) {
    	   return cantidadProductoService.getByAgenciaCategoriaTipoProducto(agencia, categoria, tipoProducto);
       }
       
       if(talla!="" && marca!="" && categoria.equals("") && tipoProducto!="" && color.equals("")) {
    	   return cantidadProductoService.getByAgenciaTallaMarcaTipoProducto(agencia, Integer.parseInt(talla), marca, tipoProducto);
       }
       
       if(talla.equals("") && marca!="" && categoria!="" && tipoProducto!="" && color.equals("")) {
    	   return cantidadProductoService.getByAgenciaMarcaCategoriaTipoProducto(agencia, marca, categoria, tipoProducto);
       }
       
       if(talla!="" && marca!="" && categoria!="" && tipoProducto!="" && color.equals("")) {
    	   return cantidadProductoService.getByAgenciaTallaMarcaCategoriaTipo(agencia, Integer.parseInt(talla), marca, categoria, tipoProducto);
       }
       
       if(talla!="" && marca.equals("") && categoria!="" && tipoProducto!="" && color.equals("")) {
    	   return cantidadProductoService.getByAgenciaTallaCategoriaTipoProducto(agencia, Integer.parseInt(talla), categoria, tipoProducto);
       }
       
       if(talla.equals("") && marca.equals("") && categoria.equals("") && tipoProducto.equals("") && color!="") {
    	   return cantidadProductoService.getByAgenciaColor(agencia, color);
       }
       
       if(talla!="" && marca.equals("") && categoria.equals("") && tipoProducto.equals("") && color!="") {
    	   return cantidadProductoService.getByAgenciaTallaColor(agencia,Integer.parseInt(talla), color);
       }
       
       if(talla.equals("") && marca!="" && categoria.equals("") && tipoProducto.equals("") && color!="") {
    	   return cantidadProductoService.getByAgenciaMarcaColor(agencia, marca, color);
       }
       
       if(talla.equals("") && marca.equals("") && categoria!="" && tipoProducto.equals("") && color!="") {
    	   return cantidadProductoService.getByAgenciaCategoriaColor(agencia, categoria, color);
       }
       
       if(talla.equals("") && marca.equals("") && categoria.equals("") && tipoProducto!="" && color!="") {
    	   return cantidadProductoService.getByAgenciaTipoProductoColor(agencia, tipoProducto, color);
       }
       
       if(talla!="" && marca.equals("") && categoria!="" && tipoProducto.equals("") && color!="") {
    	   return cantidadProductoService.getByAgenciaTallaCategoriaColor(agencia,Integer.parseInt(talla), categoria, color);
       }
       
       if(talla!="" && marca.equals("") && categoria!="" && tipoProducto!="" && color!="") {
    	   return cantidadProductoService.getByAgenciaTallaCategoriaTipoProdColor(agencia,Integer.parseInt(talla), categoria, tipoProducto, color);
       }
       
       if(talla.equals("") && marca.equals("") && categoria!="" && tipoProducto!="" && color!="") {
    	   return cantidadProductoService.getByAgenciaCategoriaTipoProductoColor(agencia, categoria, tipoProducto, color);
       }
       
       if(talla.equals("") && marca!="" && categoria!="" && tipoProducto.equals("") && color!="") {
    	   return cantidadProductoService.getByAgenciaMarcaCategoriaColor(agencia, marca, categoria, color);
       }
    	     
        return null;		
	}

}
