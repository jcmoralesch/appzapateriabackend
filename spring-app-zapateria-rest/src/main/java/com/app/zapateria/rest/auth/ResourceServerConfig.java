package com.app.zapateria.rest.auth;

import java.util.Arrays;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter{
	
	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers(HttpMethod.GET,"/api/producto/img/**","/images/**").permitAll()
		.antMatchers(HttpMethod.POST,"/api/talla").hasRole("ADMIN")
		.antMatchers(HttpMethod.POST,"/api/categoria").hasRole("ADMIN")
		.antMatchers(HttpMethod.POST,"/api/color").hasRole("ADMIN")
		.antMatchers(HttpMethod.POST,"/api/marca").hasRole("ADMIN")
		.antMatchers(HttpMethod.POST,"/api/personal").hasRole("ADMIN")
		.antMatchers(HttpMethod.GET,"/api/personal/{page}").hasRole("ADMIN")
		.antMatchers(HttpMethod.GET,"/api/personal/personal/{id}").hasRole("ADMIN")
		.antMatchers(HttpMethod.PUT,"/api/personal/update/{id}").hasRole("ADMIN")
		.antMatchers(HttpMethod.DELETE,"/api/personal/delete/{id}").hasRole("ADMIN")
		.antMatchers(HttpMethod.POST,"/api/producto").hasRole("ADMIN")
		.antMatchers(HttpMethod.POST,"/producto/upload").hasRole("ADMIN")
		.antMatchers(HttpMethod.GET,"/api/producto").hasRole("ADMIN")
		.antMatchers(HttpMethod.PUT,"/api/producto").hasRole("ADMIN")
		.antMatchers(HttpMethod.POST,"/api/proveedor").hasRole("ADMIN")
		.antMatchers(HttpMethod.GET,"/api/proveedor/**").hasRole("ADMIN")
		.antMatchers(HttpMethod.PUT,"/api/proveedor").hasRole("ADMIN")
		.antMatchers(HttpMethod.POST,"/api/tipo-producto").hasRole("ADMIN")
		.antMatchers(HttpMethod.POST,"/api/cantidad-producto").hasRole("ADMIN")
		.antMatchers(HttpMethod.GET,"/api/cantidad-producto/{nombre}").hasAnyRole("ADMIN","USER")
		.antMatchers(HttpMethod.POST,"/api/ingreso-producto").hasRole("ADMIN")
		.antMatchers(HttpMethod.GET,"/api/ingreso-producto/{fecha1}/{fecha2}").hasRole("ADMIN")
		.antMatchers(HttpMethod.GET,"/api/ingreso-producto").hasRole("ADMIN")
		.antMatchers(HttpMethod.POST,"/api/traslado-producto").hasRole("ADMIN")
		.antMatchers(HttpMethod.GET,"/api/traslado-producto").hasRole("ADMIN")
		.antMatchers(HttpMethod.GET,"/api/usuario").hasRole("ADMIN")
		.antMatchers(HttpMethod.DELETE,"/api/usuario/{id}").hasRole("ADMIN")
		.antMatchers(HttpMethod.POST,"/api/usuario").hasRole("ADMIN")
		.antMatchers(HttpMethod.POST,"/api/cliente").hasAnyRole("ADMIN","USER")
		.antMatchers(HttpMethod.GET,"/api/factura/filtrar-productos/{term}").hasAnyRole("ADMIN","USER")
		.antMatchers(HttpMethod.POST,"/api/factura").hasAnyRole("ADMIN","USER")
		.antMatchers(HttpMethod.GET,"/api/factura").hasAnyRole("ADMIN","USER")
		.antMatchers(HttpMethod.GET,"/api/factura/{id}").hasAnyRole("ADMIN","USER")
		.antMatchers(HttpMethod.GET,"/api/facura/{fecha1}/{fecha2}/{agencia}").hasRole("ADMIN")
		.antMatchers(HttpMethod.POST,"/api/traslado-entre-agencia").hasRole("ADMIN")
		.anyRequest().authenticated()
		.and().cors().configurationSource(corsConfigurationSource());//Estos metodos son para los cors
	}
	
	@Bean
    public CorsConfigurationSource corsConfigurationSource() {//Este es un filtro, de prioridad mas alta de los filtros de spring, este filro aplica para el servidor de autorizacion y de recursos.
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Arrays.asList("http://localhost:4200","*"));
        configuration.setAllowedMethods(Arrays.asList("GET","POST","DELETE","PUT","OPTIONS"));
        configuration.setAllowCredentials(true);
        configuration.setAllowedHeaders(Arrays.asList("Content-Type","Authorization"));
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);//Para todas las rutas del backend
        return source;
    }
	
	 @Bean
	 public FilterRegistrationBean<CorsFilter>corsFilter(){
		 FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<CorsFilter>(new CorsFilter(corsConfigurationSource()));
		 bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
		 return bean;
		 
	 }

}
