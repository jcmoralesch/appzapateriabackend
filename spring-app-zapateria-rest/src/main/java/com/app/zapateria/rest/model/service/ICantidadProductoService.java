package com.app.zapateria.rest.model.service;

import java.util.List;

import com.app.zapateria.rest.model.entity.Agencia;
import com.app.zapateria.rest.model.entity.CantidadProducto;
import com.app.zapateria.rest.model.entity.Producto;

public interface ICantidadProductoService {
	
	public CantidadProducto store(CantidadProducto cantidadProducto);
	public CantidadProducto findByAgenciaAndProducto(Agencia agencia,Producto producto);
	public CantidadProducto getByProductoAndAgencia(String producto,String agencia);
	public List<CantidadProducto> findProductoFilterByNombre(String term,String nombreAgencia);
	public List<CantidadProducto> getAll();
	public List<CantidadProducto> getByCantidadAll(Integer cantidad);
	public List<CantidadProducto> getByAgenciaAndCantidad(String agencia,Integer cantidad);
	public List<CantidadProducto> findByAgencia(Agencia agencia);
	public List<CantidadProducto> getByTalla(Integer talla,String agencia);
	public List<CantidadProducto> getProductoByAgenciaAndCantidad(String nombre);
	public List<CantidadProducto> getByMarca(String agencia,String marca);
	public List<CantidadProducto> getByAgenciaTallaMarca(String agencia,Integer talla,String marca);
	public List<CantidadProducto> getByAgenciaCategoria(String agencia,String categoria);
	public List<CantidadProducto> getByAgenciaTallaCategoria(String agencia,Integer talla,String categoria);
	public List<CantidadProducto> getByAgenciaMarcaCategoria(String agencia,String marca,String categoria);
	public List<CantidadProducto> getByAgenciaTallaCategoriaMarca(String agencia,Integer talla,String marca,String categoria);
	public List<CantidadProducto> getByAgenciaTipoProducto(String agencia,String tipoProducto);
	public List<CantidadProducto> getByAgenciaTallaTipoProducto(String agencia,Integer talla,String tipoProducto);
	public List<CantidadProducto> getByAgenciaMarcaTipoProducto(String agencia,String marca,String tipoProducto);
	public List<CantidadProducto> getByAgenciaCategoriaTipoProducto(String agencia,String categoria,String tipoProducto);
	public List<CantidadProducto> getByAgenciaTallaMarcaTipoProducto(String agencia,Integer talla,String marca,String tipoProducto); 
	public List<CantidadProducto> getByAgenciaMarcaCategoriaTipoProducto(String agencia,String marca,String categoria,String tipoProducto);
	public List<CantidadProducto> getByAgenciaTallaMarcaCategoriaTipo(String agencia,Integer talla,String marca,String categoria, String tipo);
	public List<CantidadProducto> getByAgenciaTallaCategoriaTipoProducto(String agencia,Integer talla,String categoria,String tipoProducto);
	public List<CantidadProducto> getByAgenciaColor(String agencia,String color);
	public List<CantidadProducto> getByAgenciaTallaColor(String agencia,Integer talla,String color);
	public List<CantidadProducto> getByAgenciaMarcaColor(String agencia,String marca,String color);
	public List<CantidadProducto> getByAgenciaCategoriaColor(String agencia,String categoria,String color);
	public List<CantidadProducto> getByAgenciaTipoProductoColor(String agencia,String tipoProducto,String color);
	public List<CantidadProducto> getByAgenciaTallaCategoriaColor(String agencia,Integer talla,String categoria,String color);
	public List<CantidadProducto> getByAgenciaTallaCategoriaTipoProdColor(String agencia,Integer talla, String categoria,String tipoProducto,String color);
	public List<CantidadProducto> getByAgenciaCategoriaTipoProductoColor(String agencia,String categoria,String tipoProducto,String color);
	public List<CantidadProducto> getByAgenciaMarcaCategoriaColor(String agencia,String marca,String categoria,String color);

	
	

}
