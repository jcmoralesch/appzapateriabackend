package com.app.zapateria.rest.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.app.zapateria.rest.model.dao.ICantidadProductoDao;
import com.app.zapateria.rest.model.entity.Agencia;
import com.app.zapateria.rest.model.entity.CantidadProducto;
import com.app.zapateria.rest.model.entity.Producto;

@Service
public class CantidadProductoServiceImpl implements ICantidadProductoService {
	
	@Autowired
	private ICantidadProductoDao cantidadProductoDao;

	@Override
	public CantidadProducto store(CantidadProducto cantidadProducto) {
		
		return cantidadProductoDao.save(cantidadProducto);
	}
	

	@Override
	public List<CantidadProducto> getAll() {
		return cantidadProductoDao.findAll();
	}

	@Override
	public CantidadProducto findByAgenciaAndProducto(Agencia agencia, Producto producto) {
		
		return cantidadProductoDao.findByAgenciaAndProducto(agencia, producto);
	}
	
	@Override
	public List<CantidadProducto> getByAgenciaAndCantidad(String agencia, Integer cantidad) {
		
		return cantidadProductoDao.findByAgenciaAndCantidad(agencia, cantidad);
	}
	
	@Override
	public List<CantidadProducto> getByCantidadAll(Integer cantidad) {
		
		return cantidadProductoDao.findByCantidadAll(cantidad);
	}
	
	@Override
	public CantidadProducto getByProductoAndAgencia(String producto,String agencia) {
	     
		return cantidadProductoDao.findByProductoAndAgencia(producto,agencia);
	}

	@Override
	public List<CantidadProducto> findByAgencia(Agencia agencia) {
		
		return cantidadProductoDao.findByAgencia(agencia);
	}

	@Override
	public List<CantidadProducto> getByTalla(Integer talla, String agencia) {
		
		return cantidadProductoDao.findByTalla(talla, agencia);
	}
	
	@Override
	public List<CantidadProducto> getProductoByAgenciaAndCantidad(String nombre) {
		
		return cantidadProductoDao.findProductoByAgenciaAndCantidad(nombre);
	}

	@Override
	public List<CantidadProducto> getByMarca(String agencia, String marca) {
		
		return cantidadProductoDao.findByMarca(agencia, marca);
	}

	@Override
	public List<CantidadProducto> getByAgenciaTallaMarca(String agencia, Integer talla, String marca) {
		
		return cantidadProductoDao.findByAgenciaTallaMarca(agencia, talla, marca);
	}

	@Override
	public List<CantidadProducto> getByAgenciaCategoria(String agencia, String categoria) {
		
		return cantidadProductoDao.findByAgenciaCategoria(agencia, categoria);
	}

	@Override
	public List<CantidadProducto> getByAgenciaTallaCategoria(String agencia, Integer talla, String categoria) {
		
		return cantidadProductoDao.findByAgenciaTallaCategoria(agencia, talla, categoria);
	}

	@Override
	public List<CantidadProducto> getByAgenciaMarcaCategoria(String agencia, String marca, String categoria) {
		
		return cantidadProductoDao.findByAgenciaMarcaCategoria(agencia, marca, categoria);
	}

	@Override
	public List<CantidadProducto> getByAgenciaTallaCategoriaMarca(String agencia, Integer talla, String marca,
			String categoria) {
		
		return cantidadProductoDao.findByAgenciaTallaCategoriaMarca(agencia, talla, marca, categoria);
	}

	@Override
	public List<CantidadProducto> getByAgenciaTipoProducto(String agencia, String tipoProducto) {
		
		return cantidadProductoDao.findByAgenciaTipoProducto(agencia, tipoProducto);
	}

	@Override
	public List<CantidadProducto> getByAgenciaTallaTipoProducto(String agencia, Integer talla, String tipoProducto) {
		
		return cantidadProductoDao.findByAgenciaTallaTipoProducto(agencia, talla, tipoProducto);
	}

	@Override
	public List<CantidadProducto> getByAgenciaMarcaTipoProducto(String agencia, String marca, String tipoProducto) {
	
		return cantidadProductoDao.findByAgenciaMarcaTipoProducto(agencia, marca, tipoProducto);
	}

	@Override
	public List<CantidadProducto> getByAgenciaCategoriaTipoProducto(String agencia, String categoria,
			String tipoProducto) {
		
		return cantidadProductoDao.findByAgenciaCategoriaTipoProducto(agencia, categoria, tipoProducto);
	}

	@Override
	public List<CantidadProducto> getByAgenciaTallaMarcaTipoProducto(String agencia, Integer talla, String marca,
			String tipoProducto) {
		
		return cantidadProductoDao.findByAgenciaTallaMarcaTipoProducto(agencia, talla, marca, tipoProducto);
	}

	@Override
	public List<CantidadProducto> getByAgenciaMarcaCategoriaTipoProducto(String agencia, String marca, String categoria,
			String tipoProducto) {
		
		return cantidadProductoDao.findByAgenciaMarcaCategoriaTipoProducto(agencia, marca, categoria, tipoProducto);
	}

	@Override
	public List<CantidadProducto> getByAgenciaTallaMarcaCategoriaTipo(String agencia, Integer talla, String marca,
			String categoria, String tipo) {
		
		return cantidadProductoDao.findByAgenciaTallaMarcaCategoriaTipo(agencia, talla, marca, categoria, tipo);
	}

	@Override
	public List<CantidadProducto> getByAgenciaTallaCategoriaTipoProducto(String agencia, Integer talla,
			String categoria, String tipoProducto) {
		
		return cantidadProductoDao.findByAgenciaTallaCategoriaTipoProducto(agencia, talla, categoria, tipoProducto);
	}

	@Override
	public List<CantidadProducto> getByAgenciaColor(String agencia, String color) {
		
		return cantidadProductoDao.findByAgenciaColor(agencia, color);
	}

	@Override
	public List<CantidadProducto> getByAgenciaTallaColor(String agencia, Integer talla, String color) {
		
		return cantidadProductoDao.findByAgenciaTallaColor(agencia, talla, color);
	}

	@Override
	public List<CantidadProducto> getByAgenciaMarcaColor(String agencia, String marca, String color) {
		
		return cantidadProductoDao.findByAgenciaMarcaColor(agencia, marca, color);
	}

	@Override
	public List<CantidadProducto> getByAgenciaCategoriaColor(String agencia, String categoria, String color) {
		
		return cantidadProductoDao.findByAgenciaCategoriaColor(agencia, categoria, color);
	}

	@Override
	public List<CantidadProducto> getByAgenciaTipoProductoColor(String agencia, String tipoProducto, String color) {
		
		return cantidadProductoDao.findByAgenciaTipoProductoColor(agencia, tipoProducto, color);
	}

	@Override
	public List<CantidadProducto> getByAgenciaTallaCategoriaColor(String agencia, Integer talla, String categoria,
			String color) {
		
		return cantidadProductoDao.findByAgenciaTallaCategoriaColor(agencia, talla, categoria, color);
	}

	@Override
	public List<CantidadProducto> getByAgenciaTallaCategoriaTipoProdColor(String agencia, Integer talla,
			String categoria, String tipoProducto, String color) {
		
		return cantidadProductoDao.findByAgenciaTallaCategoriaTipoProdColor(agencia, talla, categoria, tipoProducto, color);
	}

	@Override
	public List<CantidadProducto> getByAgenciaCategoriaTipoProductoColor(String agencia, String categoria,
			String tipoProducto, String color) {
		
		return cantidadProductoDao.findByAgenciaCategoriaTipoProductoColor(agencia, categoria, tipoProducto, color);
	}

	@Override
	public List<CantidadProducto> getByAgenciaMarcaCategoriaColor(String agencia, String marca, String categoria,
			String color) {
		
		return cantidadProductoDao.findByAgenciaMarcaCategoriaColor(agencia, marca, categoria, color);
	}


	@Override
	public List<CantidadProducto> findProductoFilterByNombre(String term, String nombreAgencia) {
		
		return cantidadProductoDao.findProductoFilterByNombre(term, nombreAgencia);
	}

}
