package com.app.zapateria.rest.model.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.app.zapateria.rest.model.entity.TipoProducto;

public interface ITipoProductoDao extends JpaRepository<TipoProducto, Long> {

}
