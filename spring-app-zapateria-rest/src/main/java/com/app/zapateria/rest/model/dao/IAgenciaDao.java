package com.app.zapateria.rest.model.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.app.zapateria.rest.model.entity.Agencia;

public interface IAgenciaDao extends JpaRepository<Agencia, Long> {
	
	public Agencia findByNombre(String nombre);

}
