insert into agencias(nombre,ubicacion) values('Diamante','Chichicastenango');
insert into agencias(nombre,ubicacion) values('Diamante 2','Chichicastenango');
insert into agencias(nombre,ubicacion) values('Diamante 3','Quiche');

insert into personals(identificacion,nombre,apellido,direccion,telefono,agencia_id)values('21010 0122 00','Brandon','Morales','Ciudad','35353535',1);
insert into personals(identificacion,nombre,apellido,direccion,telefono,agencia_id)values('21010 3555 50','Eimy','Morales','Ciudad','352511111',3);

insert into usuarios(enabled,username,password,personal_id)values(1,'admin','$2a$10$VI9EKCr7uZ2g5NUPC3g1tOtvpeSStDS3TLbCTB8eRbZ.XClcFRWCK',1);
insert into usuarios(enabled,username,password,personal_id)values(1,'user','$2a$10$/F0S604Jql4cpXAVijE4SOJPq4BTrx17Tbg.bg5kTRkW03MXPciLq',2);

insert into roles(nombre) values('ROLE_USER');
insert into roles(nombre) values('ROLE_ADMIN');
insert into roles(nombre) values('ROLE_GESTOR');

insert into usuarios_role(usuario_id,role_id)values(1,2);
insert into usuarios_role(usuario_id,role_id)values(2,1);

insert into categorias(categoria)values('Caballeros');
insert into categorias(categoria)values('Damas');
insert into categorias(categoria)values('Ninios');

insert into colors(color)values('NEGRO');
insert into colors(color)values('BLANCO');

insert into marcas(marca)values('REBOOK');
insert into marcas(marca)values('D-COAST');
insert into marcas(marca)values('CIFUENTES');

insert into empresas(nombre,direccion,telefono)values('RIOS DE GUATEMALA','CIUDAD DE GUATEMALA','22120202');
insert into empresas(nombre,direccion,telefono)values('CALZADO ROY','CIUDAD QUETZAL','22120202');
insert into empresas(nombre,direccion,telefono)values('CALZADO NACIONAL','CIUDAD DE GUATEMALA','22120202');

insert into proveedors(identificacion,nombre,apellido,status,email,telefono,empresa_id)values('12000 0000','Bernabe','Castillo','A','bernabe@mail.com','25412010',1);
insert into proveedors(identificacion,nombre,apellido,status,email,telefono,empresa_id)values('12000 4444','Fernando','Botran','A','fer@mail.com','2541254',2);
insert into proveedors(identificacion,nombre,apellido,status,email,telefono,empresa_id)values('12000 6666','Jorge','Novella','A','jorge@mail.com','25412666',3);

insert into tallas(talla)values(25);
insert into tallas(talla)values(27);
insert into tallas(talla)values(29);
insert into tallas(talla)values(31);
insert into tallas(talla)values(33);
insert into tallas(talla)values(35);
insert into tallas(talla)values(37);
insert into tallas(talla)values(39);
insert into tallas(talla)values(41);

insert into tipo_productos(tipo_producto)values('DEPORTIVO');
insert into tipo_productos(tipo_producto)values('CASUAL');
insert into tipo_productos(tipo_producto)values('ESCOLAR');

insert into productos(codigo,precio,fecha_registro,precio_venta,status,categoria_id,color_id,marca_id,proveedor_id,talla_id,tipo_producto_id,alerta)values('ARC01',200.00,'2019/09/05','300.00','D',1,1,1,1,1,1,2);
insert into productos(codigo,precio,fecha_registro,precio_venta,status,categoria_id,color_id,marca_id,proveedor_id,talla_id,tipo_producto_id,alerta)values('ARC02',200.00,'2019/09/05','300.00','D',2,2,1,2,2,2,2);
insert into productos(codigo,precio,fecha_registro,precio_venta,status,categoria_id,color_id,marca_id,proveedor_id,talla_id,tipo_producto_id,alerta)values('ARC03',200.00,'2019/09/05','300.00','D',3,1,3,3,3,3,2);
insert into productos(codigo,precio,fecha_registro,precio_venta,status,categoria_id,color_id,marca_id,proveedor_id,talla_id,tipo_producto_id,alerta)values('ARC04',200.00,'2019/09/05','300.00','D',1,2,1,1,4,1,2);
insert into productos(codigo,precio,fecha_registro,precio_venta,status,categoria_id,color_id,marca_id,proveedor_id,talla_id,tipo_producto_id,alerta)values('ARC05',200.00,'2019/09/05','300.00','D',2,1,2,2,5,2,2);
insert into productos(codigo,precio,fecha_registro,precio_venta,status,categoria_id,color_id,marca_id,proveedor_id,talla_id,tipo_producto_id,alerta)values('ARC06',200.00,'2019/09/05','300.00','D',3,2,3,3,6,3,2);
insert into productos(codigo,precio,fecha_registro,precio_venta,status,categoria_id,color_id,marca_id,proveedor_id,talla_id,tipo_producto_id,alerta)values('ARC07',200.00,'2019/09/05','300.00','D',1,1,1,1,7,1,2);
insert into productos(codigo,precio,fecha_registro,precio_venta,status,categoria_id,color_id,marca_id,proveedor_id,talla_id,tipo_producto_id,alerta)values('ARC08',200.00,'2019/09/05','300.00','D',2,2,2,2,8,2,2);
insert into productos(codigo,precio,fecha_registro,precio_venta,status,categoria_id,color_id,marca_id,proveedor_id,talla_id,tipo_producto_id,alerta)values('ARC09',200.00,'2019/09/05','300.00','D',3,1,3,3,9,3,2);
insert into productos(codigo,precio,fecha_registro,precio_venta,status,categoria_id,color_id,marca_id,proveedor_id,talla_id,tipo_producto_id,alerta)values('ARC010',200.00,'2019/09/05','300.00','D',1,2,1,1,1,1,2);
insert into productos(codigo,precio,fecha_registro,precio_venta,status,categoria_id,color_id,marca_id,proveedor_id,talla_id,tipo_producto_id,alerta)values('ARC011',200.00,'2019/09/05','300.00','D',2,1,2,2,2,2,2);
insert into productos(codigo,precio,fecha_registro,precio_venta,status,categoria_id,color_id,marca_id,proveedor_id,talla_id,tipo_producto_id,alerta)values('ARC012',200.00,'2019/09/05','300.00','D',3,2,3,3,3,3,2);
insert into productos(codigo,precio,fecha_registro,precio_venta,status,categoria_id,color_id,marca_id,proveedor_id,talla_id,tipo_producto_id,alerta)values('ARC013',200.00,'2019/09/05','300.00','D',1,1,1,1,4,1,2);
insert into productos(codigo,precio,fecha_registro,precio_venta,status,categoria_id,color_id,marca_id,proveedor_id,talla_id,tipo_producto_id,alerta)values('ARC014',200.00,'2019/09/05','300.00','D',2,2,1,2,5,2,2);
insert into productos(codigo,precio,fecha_registro,precio_venta,status,categoria_id,color_id,marca_id,proveedor_id,talla_id,tipo_producto_id,alerta)values('ARC015',200.00,'2019/09/05','300.00','D',3,1,1,3,6,3,2)












